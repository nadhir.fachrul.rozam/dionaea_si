require('dotenv').config();
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

mongoose.connect(process.env.DATABASE, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;
mongoose.connection
.on('connected', () => {
    console.log(`Mongoose connection open on ${process.env.DATABASE}`);
})
.on('error', (err) => {
    console.log(`Connection error: ${err.message}`);
  });
  
//include all models file  
const modelsPath = require("path").join(__dirname, "models");
require("fs").readdirSync(modelsPath).forEach(function(file) {
    require("./models/" + file);
});

const app = require('./app');
var server = require('http').Server(app);
var io = global.io = require('socket.io')(server);
app.set('socketio', io);

server.listen(3000,() =>{
    console.log(`Express is running op port ${server.address().port}`);
});
