const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var dcerpcbind = Schema({
    _id: Schema.Types.ObjectId,
    dcerpcbind: {
        type: Number,
        trim: true,
    },
    dcerpcbind_uuid: {
        type: String,
        trim: true,
    },
    dcerpcbind_transfersyntax: {
        type: String,
        trim: true,
    },
});

var dcerpcrequest = Schema({
    _id: Schema.Types.ObjectId,
    dcerpcrequest: {
        type: Number,
        trim: true,
    },
    dcerpcrequest_uuid: {
        type: String,
        trim: true,
    },
    dcerpcrequest_opnum: {
        type: Number,
        trim: true,
    },
});

var login = Schema({
    _id: Schema.Types.ObjectId,
    login: {
        type: Number,
        trim: true,
    },
    login_username: {
        type: String,
        trim: true,
    },
    login_password: {
        type: String,
        trim: true,
    },
});

var mssql_fingerprint = Schema({
    _id: Schema.Types.ObjectId,
    mssql_fingerprint: {
        type: Number,
        trim: true,
    },
    mssql_fingerprint_hostname: {
        type: String,
        trim: true,
    },
    mssql_fingerprint_appname: {
        type: String,
        trim: true,
    },
    mssql_fingerprint_cltintname: {
        type: String,
        trim: true,
    },
});

var mssql_command = Schema({
    _id: Schema.Types.ObjectId,
    mssql_command: {
        type: Number,
        trim: true,
    },
    mssql_command_status: {
        type: String,
        trim: true,
    },
    mssql_command_cmd: {
        type: String,
        trim: true,
    },
});

var mysql_command = Schema({
    _id: Schema.Types.ObjectId,
    mysql_command: {
        type: Number,
        trim: true,
    },
    mysql_command_cmd: {
        type: String,
        trim: true,
    },
   
});

var mysqlDetail = new Schema({
    mysql_command: {
        type: Number,
        trim: true,
    },
    
    mysql_command_arg_index: {
        type: Number,
        trim: true,
    },
    mysql_command_arg_data: {
        type: String,
        trim: true,
    }, 
    mysql_command_op_name: {
        type: String,
        trim: true,
    },
   
});

var mysql_command_op = Schema({
    _id: Schema.Types.ObjectId,
    mysql_command_op: {
        type: Number,
        trim: true,
    },
    mysql_command_cmd: {
        type: Number,
        trim: true,
    },
    mysql_command_op_name: {
        type: String,
        trim: true,
    },
   
});

var mysql_command_arg = Schema({
    _id: Schema.Types.ObjectId,
    mysql_command_arg: {
        type: Number,
        trim: true,
    },
    mysql_command: {
        type: Number,
        trim: true,
    },
    mysql_command_arg_index: {
        type: Number,
        trim: true,
    },
    mysql_command_arg_data: {
        type: String,
        trim: true,
    },
   
});

var download = Schema({
    _id: Schema.Types.ObjectId,
    download: {
        type: Number,
        trim: true,
    },
    download_url: {
        type: String,
        trim: true,
    },
    download_md5_hash: {
        type: String,
        trim: true,
    },
   
});
var offer = Schema({
    _id: Schema.Types.ObjectId,
    offer: {
        type: Number,
        trim: true,
    },
    offer_url: {
        type: String,
        trim: true,
    },
   
});

var dionaeaembeddedSchema = Schema({
    _id: Schema.Types.ObjectId,
    connection: {
        type: Number,
        trim: true,
    },
    connection_type: {
        type: String,
        trim: true,
    },
    connection_transport: {
        type: String,
        trim: true,
    },
    connection_protocol: {
        type: String,
        trim: true,
    },
    connection_timestamp: {
        type: Number,
        trim: true,
        get: toDate,
        set: val => Math.round(val.getTime() / 1000),
    },
    connection_root: {
        type: Number,
        trim: true,
    },
    connection_parent: {
        type: Number,
        trim: true,
    },
    local_host: {
        type: String,
        trim: true,
    },
    local_port: {
        type: Number,
        trim: true,
    },
    remote_host: {
        type: String,
        trim: true,
    },
    remote_hostname: {
        type: String,
        trim: true,
    },
    remote_port: {
        type: Number,
        trim: true,
    },
    dcerpcbind : dcerpcbind,
    dcerpcrequest : dcerpcrequest,
    login : [login],
    mssql_fingerprint : mssql_fingerprint,
    mssql_command : mssql_command,
    mysql_command : mysql_command,
    mysql_command_op : mysql_command_op,
    mysql_command_arg : mysql_command_arg,
    download : [download],
    offer : offer,
    mysqlDetail : [mysqlDetail],


}, { collection: 'dionaeaembedded' });

function toDate(epoch) {
    var iso = new Date(parseInt(epoch) * 1000).toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
    return iso[1] + ' ' + iso[2];
}

module.exports = mongoose.model("Dionaeaembedded", dionaeaembeddedSchema)