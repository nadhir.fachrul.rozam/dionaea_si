// Call the dataTables jQuery plugin

function getUrl(val) {
  console.log(val.text)
  $.ajax({
    url: '/scan_url',
    method: 'GET',
    data: { url: val.text },
    // dataType: 'json',
    success: function (d) {
      // console.log(d);
      var win = window.open(d, '_blank');
      win.focus();
    }
  });
}

$(document).ready(function () {
  $('#dataTable').DataTable({
    "processing": true,
    "pagingType": "input",
    "serverSide": true,
    "ajax": {
      url: "/file_download/data",
      timeout: 3600000
    },
    "columnDefs": [{
      "targets": 3,
      // "data": "download_link",
      "render": function (data, type, row, meta) {
        return '<a onclick="getUrl(this)" href="https://www.virustotal.com/gui/search/'+data+'" target="_blank">' + data + '</a>';
      }
    }],
    "initComplete": function () {
      $('#dataTable_filter input').unbind();
      $('#dataTable_filter input').bind('keyup', function(e) {
          if(e.keyCode == 13) {
              table.search( this.value ).draw();
          }
      }); 
    }
  });
});
