// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var max = 0;
var steps = 10;
var chartData = {};
var tableData = {};
var chart = 0;
var tableChart = 0;

function respondCanvas() {
  if(chart != 0 ) {
    chart.destroy();
  }
  var c = $('#myBarChart');
  var ctx = c.get(0).getContext("2d");
  var container = c.parent();

  var $container = $(container);

  c.attr('width', $container.width()); //max width

  c.attr('height', $container.height()); //max height                   

  //Call a function to redraw other content (texts, images etc)
  chart = new Chart(ctx, {
    type: 'bar',
    data: chartData,
    options: {
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          },
        }],
        yAxes: [{
          ticks: {
            min: 0,
            max: max,
            stepSize: max >= 5 ? Math.ceil(max/steps) : 1
          },
          gridLines: {
            display: true
          }
        }],
      },
      legend: {
        display: false
      }
    }
  });
}

socket.on('classification', function (data) {
  console.log(data);
  chartData = data;

  max = Math.max(...data.datasets[0]['data']);
  steps = 5;

  respondCanvas();
  // socket.emit('my other event', { my: 'data' });
});

socket.on('classification_table', function (data) {
  console.log(data);
  tableData = data;
  addTable();
});

function fetchChart(index) {
  if(chart != 0 ) {
    chart.destroy();
  }

  if(tableChart != 0 ) {
    tableChart.destroy();
  }

  if ($('#tableChart').length) {
    $('#tableChart').remove();
  }
  
  $.ajax({
    url: '/classification/chart/',
    method: 'GET',
    data: { option: index },
    dataType: 'json',
    success: function (d) {
      if ($("#tableChart").length != 0) {
        $("#tableChart").dataTable.destroy();
      }
      console.log(d);
    }
  });
}

function addTable() {
  var myTableDiv = document.getElementById("chart_table");

  var table = document.createElement('TABLE');
  table.border = '1';
  table.setAttribute('class', 'table table-bordered');
  table.setAttribute('id', 'tableChart');
  
  var tableBody = document.createElement('THEAD');
  var tr = document.createElement('TR');
  tableBody.appendChild(tr);
  var td = document.createElement('TH');
  td.appendChild(document.createTextNode("Classification"));
  tr.appendChild(td);
  var td = document.createElement('TH');
  td.appendChild(document.createTextNode("Counts"));
  tr.appendChild(td);
  table.appendChild(tableBody);
  
  var tableBody = document.createElement('TBODY');
  table.appendChild(tableBody);

  myTableDiv.appendChild(table);
  
  tableChart = $('#tableChart').DataTable({
    "searching": false,
    "paging":   false,
    "lengthChange": true,
  });

  for (const [key, value] of Object.entries(tableData)) {
    tableChart.row.add([key, value]);
  }
  tableChart.draw();
}