// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var max = 0;
var steps = 10;
var chartData = {};
var chart = 0;

function respondCanvas() {
  if(chart != 0 ) {
    chart.destroy();
  }
  var c = $('#myBarChart');
  var ctx = c.get(0).getContext("2d");
  var container = c.parent();

  var $container = $(container);

  c.attr('width', $container.width()); //max width

  c.attr('height', $container.height()); //max height                   

  //Call a function to redraw other content (texts, images etc)
  chart = new Chart(ctx, {
    type: 'bar',
    data: chartData,
    options: {
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          },
        }],
        yAxes: [{
          ticks: {
            min: 0,
            max: max,
            stepSize: max >= 5 ? Math.ceil(max/steps) : 1
          },
          gridLines: {
            display: true
          }
        }],
      },
      legend: {
        display: false
      }
    }
  });
}

socket.on('file_download', function (data) {
  console.log(data);
  chartData = data;

  max = Math.max(...data.datasets[0]['data']);
  steps = 5;

  respondCanvas();
  // socket.emit('my other event', { my: 'data' });
});

function fetchChart(index) {
  if(chart != 0 ) {
    chart.destroy();
  }
  
  $.ajax({
    url: '/file_download/chart/',
    method: 'GET',
    data: { option: index },
    dataType: 'json',
    success: function (d) {
      console.log(d);
    }
  });
}
