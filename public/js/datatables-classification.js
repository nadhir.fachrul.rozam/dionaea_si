// Call the dataTables jQuery plugin
$(document).ready(function() {
  var table = $('#dataTable').DataTable({
      "processing": true,
      "pagingType": "input",
      "serverSide": true,
      "ajax": {
        url: "/classification/data",
        timeout: 3600000
      },
      "columnDefs": [{
        "targets": 2,
        // "data": "download_link",
        "render": function (data, type, row, meta) {
          var date = new Date(parseInt(data)*1000);
          var iso = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/)
          return iso[1] + ' ' + iso[2];
        }
      }],
      "initComplete": function () {
        $('#dataTable_filter input').unbind();
        $('#dataTable_filter input').bind('keyup', function(e) {
            if(e.keyCode == 13) {
                table.search( this.value ).draw();
            }
        }); 

        this.api().columns().every( function () {
            var column = this;
            if (table.column(-1).index() == column.index()) {
              console.log("Same");
            var select = $('<select><option value=""></option></select>')
                .appendTo( $(column.footer()) )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            ['Tautological','Batch Attack','Stored Procedure','Alternate Encoding',
             'Second Order Injection','Ilegal/Incorrect Query Injection','Unknown'].sort().forEach( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
          }
        } );
    }
  });
});
