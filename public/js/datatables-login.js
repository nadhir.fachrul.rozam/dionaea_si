// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
      "processing": true,
      "pagingType": "input",
      "serverSide": true,
      "ajax": {
        url: "/login/data",
        timeout: 3600000
      },
      "initComplete": function () {
        $('#dataTable_filter input').unbind();
        $('#dataTable_filter input').bind('keyup', function(e) {
            if(e.keyCode == 13) {
                table.search( this.value ).draw();
            }
        }); 
      }
  });
});


