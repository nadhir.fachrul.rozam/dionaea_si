const express = require('express');
const mongoose = require('mongoose');
var geoip = require('geoip-lite');
const router = express.Router();
const { body, validationResult } = require('express-validator');
const CircularJSON = require('circular-json');
const VirusTotalApi = require('virustotal-api')
const virusTotal = new VirusTotalApi('73b76057764fac3e5723cb1c121469a5529c37ee580b776e7d07344ffc7dff89')

// var server = require('http').Server(express());
// var io = require('socket.io')(server);

const Dionaeaembedded = mongoose.model('Dionaeaembedded');

// io.on('connection', function (socket) {
//     console.log("connection server");
    
//     socket.emit('news', { hello: 'world' });
//     socket.on('my other event', function (data) {
//       console.log(data);
//     });
// })

router.get('/', (req, res) => {
    var io = req.app.get('socketio');
    io.on('connection', function (socket) {
    console.log("connection server");
    
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
})
    res.render('dashboard/index', { title: 'Dashboard', active: 'dashboard' });
    Dionaeaembedded.find({'local_port': 3306}).countDocuments().exec((err, result) => {
        io.emit('news', {key:'value'})
        console.log("hitt it");
    })
})

router.get('/classification/data', async (req, res) => {
    //TODO sorting
    console.debug(req.query);
    var totalCount;
    var recordsFiltered;
    var start = parseInt(req.query['start']);
    var limit = parseInt(req.query['length']);
    // totalCount = await Dionaeaembedded.find({'local_port': 3306}).countDocuments();
    // recordsFiltered = await Dionaeaembedded.find({'local_port': 3306}).countDocuments();

    var sortOrder = req.query['order'][0].dir == 'desc' ? "-" : "";
    var sortColumn = req.query['order'][0].column;
    var sortColumnName;
    console.log(sortOrder);
    switch (sortColumn) {
        case '0':
            sortColumnName = '_id';
            break;
        case '1':
            sortColumnName = 'connection';
            break;
        case '2':
            sortColumnName = 'connection_timestamp';
            break;
        case '3':
            sortColumnName = 'remote_host';
            break;
        case '4':
            sortColumnName = 'local_port';
            break;
        case '5':
            sortColumnName = 'mysql_command_args_data';
            break;
        case '6':
            sortColumnName = 'classification';
            break;
        default:
            sortColumnName = '_id';
            break;
    }
    console.log(sortColumnName);

    function Comparator(a, b) {
        var col = parseInt(sortColumn);
        if (typeof a[col] === 'number')
            return a[col] > b[col];
        return a[col].toString().localeCompare(b[col].toString());
    }

    var kuda = Dionaeaembedded.find({'local_port': 3306});
        kuda.lean()
            .exec((err, result) => {
                console.log(result.length);
                _result = result
                .reduce(function (_return, value) {
                    value.mysqlDetail.forEach(element => {
                        var syntax = (element != null && element.mysql_command_arg_data != undefined) ? element.mysql_command_arg_data : '';
                        
                        if (syntax.match(/(\W|^)(or|OR|oR|Or)\s+.*(=)(\s+|)/i)) {
                            value.classification = "Tautological";
                        } else if (syntax.match(/;(\W|)(\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)/i)) {
                            value.classification = "Batch Attack";
                        } else if (syntax.match(/^create( +function|procedure)/i)) {
                            value.classification = "Stored Procedure";
                        } else if (syntax.match(/concat(_ws)|(un)?(hex+)\W|substring\W|(([a-fA-F]|[0-9]){2}){10}/i)) {
                            value.classification = "Alternate Encoding";
                        } else if (syntax.match(/INSERT(\sINTO)|UPDATE(\s\w*)(\sSET\s)/i)) {
                            value.classification = "Second Order Injection";
                        } else if (syntax.match(/(')\W(\b(OR|ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)/i)) {
                            value.classification = "Ilegal/Incorrect Query Injection";
                        } else {
                            value.classification = "Unknown";
                        }
                        // value.connection = value.mysql_command_ref.connection;
                        // value.connection_timestamp = value.mysql_command_ref.connection_ref.connection_timestamp;
                        // value.remote_host = value.mysql_command_ref.connection_ref.remote_host;
                        // value.local_port = value.mysql_command_ref.connection_ref.local_port;
                        delete value.mysql_command_arg;
                        delete value.mysql_command;
                        delete value.mysql_command_arg_index;
                        delete value.mysql_command_ref;
                        if (syntax != '') {
                            _return.push([value._id, value.connection, value.connection_timestamp,
                                value.remote_host+" - "+geoip.lookup(value.remote_host).country, value.local_port, syntax, value.classification])
                        }
                    });
                    // if (_return.length<1) {
                    //     _return = [value._id, value.connection, value.connection_timestamp,
                    //         value.remote_host+" - "+geoip.lookup(value.remote_host).country, value.local_port, '', 'Unknown']
                    // }
                    return _return;
                },[]);
                totalCount = _result.length;
                
                //here implement filter
                if (req.query['columns'][6].search.value != '') {
                    _result = _result.reduce(function (_return, value) {

                        if (value[6].match(new RegExp(req.query['columns'][6].search.value,'gm'))) {
                            _return.push(value);
                        }
                        return _return;
                    },[]);
                }
                //here implement filter
                if (req.query.search.value != '') {
                    _result = _result.reduce(function (_return, value) {
                        for (let index = 0; index < value.length; index++) {
                            var element = value[index];

                            if (element.toString().match(new RegExp(req.query.search.value,'gm'))) {
                                _return.push(value);
                                break;
                            }
                        }
                        return _return;
                    },[]);
                }
                recordsFiltered = _result.length;
                _result.sort(Comparator.bind(sortColumn));
                if (sortOrder == '-') _result.reverse();
                _result = _result.slice(start, start + limit);
                console.log(_result.length)
                res.send({
                    "draw": req.query['draw'],
                    "recordsTotal": totalCount,
                    "recordsFiltered": recordsFiltered,
                    "data": _result
                })
            })
    


})

router.get('/classification', (req, res) => {
    res.render('classification/index', { title: 'Classification', active: 'classification' });
})

router.get('/file_download/data', async (req, res) => {
    console.debug(req.query);
    var skip = parseInt(req.query['start']);
    var limit = parseInt(req.query['length']);
    var totalCount, filteredCount;
    // totalCount = await Dionaeaembedded.find().or([{'local_port': 21}, {'local_port': 445}]).countDocuments({});
    // .paginate({}, { offset: skip, limit: limit }).
    // filteredCount = await Dionaeaembedded.find().or([{'local_port': 21}, {'local_port': 445}]).countDocuments({});
    var sortOrder = req.query['order'][0].dir == 'desc' ? "-" : "";
    var sortColumn = req.query['order'][0].column;
    var sortColumnName;
    console.log(sortOrder);
    switch (sortColumn) {
        case '0':
            sortColumnName = '_id';
            break;
        case '1':
            sortColumnName = 'connection';
            break;
        case '2':
            sortColumnName = 'remote_host';
            break;
        case '3':
            sortColumnName = 'file_download';
            break;
        default:
            sortColumnName = '_id';
            break;
    }
    console.log(sortColumnName);

    function Comparator(a, b) {
        var col = parseInt(sortColumn);
        if (typeof a[col] === 'number')
            return a[col] > b[col];
        return a[col].toString().localeCompare(b[col].toString());
    }

    var kuda = Dionaeaembedded.find().or([{'local_port': 21}, {'local_port': 445}]);
        kuda.lean()
        .exec((err, result) => {
            var _result = result.map(function (value) {
                var download_md5_hash = (value.download[0] != null && value.download[0].download_md5_hash != undefined)? value.download[0].download_md5_hash : '';
                return [value._id, value.connection, value.remote_host+" - "+geoip.lookup(value.remote_host).country, download_md5_hash];
            });
            totalCount = _result.length;
            console.info(result);
            console.info(err);
            // console.info(result.docs);

            if (req.query.search.value != '') {
                _result = _result.reduce(function (_return, value) {
                    for (let index = 0; index < value.length; index++) {
                        var element = value[index];
                        
                        if (element.toString().match(new RegExp(req.query.search.value,'gm'))) {
                            _return.push(value);
                            break;
                        }
                    }
                    return _return;
                },[]);
            }
            filteredCount = _result.length;
            _result.sort(Comparator.bind(sortColumn));
            if (sortOrder == '-') _result.reverse();
            _result = _result.slice(skip, skip + limit);

            res.send(CircularJSON.stringify({
                "draw": req.query['draw'],
                "recordsTotal": totalCount,
                "recordsFiltered": filteredCount,
                "data": _result
            }))
        })
    
})

router.get('/file_download', (req, res) => {
    res.render('file_download/index', { title: 'File Download', active: 'file_download' });
})

router.get('/login/data', async (req, res) => {
    console.debug(req.query);
    var skip = parseInt(req.query['start']);
    var limit = parseInt(req.query['length']);
    var totalCount, filteredCount;
    // totalCount = await Dionaeaembedded.find({'local_port': 3306}).countDocuments();
    // .paginate({}, { offset: skip, limit: limit }).
    // filteredCount = await Dionaeaembedded.find({'local_port': 3306}).countDocuments();

    var sortOrder = req.query['order'][0].dir == 'desc' ? "-" : "";
    var sortColumn = req.query['order'][0].column;
    var sortColumnName;
    console.log(sortOrder);
    switch (sortColumn) {
        case '0':
            sortColumnName = '_id';
            break;
        case '1':
            sortColumnName = 'connection';
            break;
        case '2':
            sortColumnName = 'remote_host';
            break;
        case '3':
            sortColumnName = 'login_username';
            break;
        case '4':
            sortColumnName = 'login_password';
            break;
        default:
            sortColumnName = '_id';
            break;
    }
    console.log(sortColumnName);

    function Comparator(a, b) {
        var col = parseInt(sortColumn);
        if (typeof a[col] === 'number')
            return a[col] > b[col];
        return a[col].toString().localeCompare(b[col].toString());
    }

    var kuda = Dionaeaembedded.find({'local_port': 3306});
        kuda.lean()
        .exec((err, result) => {
            var _result = result.map(function (value) {
                var username = (value.login[0] != null && value.login[0].login_username != undefined) ? value.login[0].login_username : '';
                var password = (value.login[0] != null && value.login[0].login_password != undefined) ? value.login[0].login_password : '';
                
                return [value._id, value.connection, value.remote_host+" - "+geoip.lookup(value.remote_host).country, username, password];
            });
            totalCount = _result.length;
            // console.info(result);
            // console.info(result.docs);
            if (req.query.search.value != '') {
                _result = _result.reduce(function (_return, value) {
                    for (let index = 0; index < value.length; index++) {
                        var element = value[index];
                        
                        if (element.toString().match(new RegExp(req.query.search.value,'gm'))) {
                            _return.push(value);
                            break;
                        }
                    }
                    return _return;
                },[]);
            }
            filteredCount = _result.length;
            _result.sort(Comparator.bind(sortColumn));
            if (sortOrder == '-') _result.reverse();
            _result = _result.slice(skip, skip + limit);
            res.send(CircularJSON.stringify({
                "draw": req.query['draw'],
                "recordsTotal": totalCount,
                "recordsFiltered": filteredCount,
                "data": _result
            }))
        })
    })

router.get('/login', (req, res) => {
    res.render('login/index', { title: 'Login', active: 'login' });
})

router.get('/login/chart', (req, res) => {
    var option = req.query['option'];
    console.log(option)
    var labels, data, _result;
    switch (option) {
        case '1':
            console.log('hitt option 1');
            
            Dionaeaembedded.find({'local_port': 3306}).select('connection').lean().exec((err, result) => {
                _result = result.map(function (value) {
                    return value.connection;
                });
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('login', data);
                console.log('emit login option 1');
            })
            res.send('ok');
            break;
        case '2':
            console.log('hitt option 2');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                // console.info(result);
                _result = result.map(function (value) {
                    return value.remote_host;
                });
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('login', data);
                console.log('emit login option 2');
            });
            res.send(data);
            break;
        case '3':
            console.log('hitt option 3');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                _result = result.map(function (value) {
                    var username = (value.login[0] != null && value.login[0].login_username != undefined) ? value.login[0].login_username : '';
                    
                    return username;
                });
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('login', data);
                console.log('emit login option 3');
            });
            res.send('ok');
            break;
        case '4':
            console.log('hitt option 4');
            
            Dionaeaembedded.find({'local_port': 3306}).lean().exec((err, result) => {
                _result = result.map(function (value) {
                    var password = (value.login[0] != null && value.login[0].login_password != undefined) ? value.login.login_password : '';
                    return password;
                });

                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('login', data);
                console.log('emit login option 4');
            });
            res.send('ok');
            break;
        default:
            res.send('error');
            break;
    }
})

router.get('/file_download/chart', (req, res) => {
    var option = req.query['option'];
    var io = req.app.get('socketio');
    io.emit('news', {key:'value'});
    console.log(option)
    var labels, data, _result;
    switch (option) {
        case '1':
            console.log('option 1');
            Dionaeaembedded.find().or([{'local_port': 21}, {'local_port': 445}]).lean().exec((err, result) => {
                _result = result.map(function (value) {
                    return value.connection;
                });
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('file_download', data);
                console.log('emit file_download option 1');
                
            })
            res.send('ok');
            break;
        case '2':
            console.log('option 2');
            Dionaeaembedded.find().or([{'local_port': 21}, {'local_port': 445}]).lean()
            .exec((err, result) => {
                // console.info(result);
                _result = result.map(function (value) {
                    return value.remote_host;
                });
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('file_download', data);
                console.log('emit file_download option 2');
            });
            res.send('ok');
            break;
        case '3':
            Dionaeaembedded.find().or([{'local_port': 21}, {'local_port': 445}]).lean()
            .exec((err, result) => {
                _result = result.map(function (value) {
                    var download_md5_hash = (value.download[0] != null && value.download[0].download_md5_hash != undefined)? value.download[0].download_md5_hash : '';
                    return download_md5_hash;
                });
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('file_download', data);
                console.log('emit file_download option 3');
            });
            res.send('ok');
            break;
        default:
            res.send('error');
            break;
    }
})

router.get('/classification/chart', (req, res) => {
    var option = req.query['option'];
    console.log(option)
    var labels, data, _result;
    switch (option) {
        case '1':
            console.log('hitt option 1');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                // console.info(result);
                _result = result.map(function (value) {
                    return value.connection;
                });
                // console.log(_result);
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('classification', data);
                console.log('emit classification option 1');
            });
            res.send('ok');
            break;
        case '2':
            console.log('hitt option 2');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                // console.info(result);
                _result = result.map(function (value) {
                    return value.remote_host;
                });
                // console.log(_result);
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('classification', data);
                console.log('emit classification option 2');
            });
            res.send('ok');
            break;
        case '3':
            console.log('hitt option 3');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                // console.info(result);
                _result = result.map(function (value) {
                    var iso = new Date(parseInt(value.connection_timestamp) * 1000).toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
                    return iso[1] + ' ' + iso[2];
                });
                // console.log(_result);
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});
                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('classification', data);
                console.log('emit classification option 3');
            });
            res.send('ok');
            break;
        case '4':
            console.log('hitt option 4');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                // console.info(result);
                _result = result
                .reduce(function (_return, value) {
                    value.mysqlDetail.forEach(element => {
                        var syntax = (element != null && element.mysql_command_arg_data != undefined) ? element.mysql_command_arg_data : '';
                                               // value.connection = value.mysql_command_ref.connection;
                        // value.connection_timestamp = value.mysql_command_ref.connection_ref.connection_timestamp;
                        // value.remote_host = value.mysql_command_ref.connection_ref.remote_host;
                        // value.local_port = value.mysql_command_ref.connection_ref.local_port;
                        delete value.mysql_command_arg;
                        delete value.mysql_command;
                        delete value.mysql_command_arg_index;
                        delete value.mysql_command_ref;
                        if (syntax != '') {
                            _return.push(syntax);
                        }
                    });
                    // if (_return.length<1) {
                    //     _return = [value._id, value.connection, value.connection_timestamp,
                    //         value.remote_host+" - "+geoip.lookup(value.remote_host).country, value.local_port, '', 'Unknown']
                    // }
                    return _return;
                },[]);
                // console.log(_result);
                labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});

                data = Object.values(labels).sort(function (a, b) { return a - b }).reverse().slice(0, 6);
                labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse().slice(0, 6);
                data = {
                    labels: labels,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "rgba(2,117,216,1)",
                        borderColor: "rgba(2,117,216,1)",
                        data: data,
                    }],
                };
                io.emit('classification', data);
                console.log('emit classification option 4');
            });
            res.send('ok');
            break;
        case '5':
            console.log('hitt option 5');
            
            Dionaeaembedded.find({'local_port': 3306}).lean()
            .exec((err, result) => {
                    // console.info(result);
                    _result = result
                .reduce(function (_return, value) {
                    value.mysqlDetail.forEach(element => {
                        var syntax = (element != null && element.mysql_command_arg_data != undefined) ? element.mysql_command_arg_data : '';
                        
                        if (syntax.match(/(\W|^)(or|OR|oR|Or)\s+.*(=)(\s+|)/i)) {
                            value.classification = "Tautological";
                        } else if (syntax.match(/;(\W|)(\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)/i)) {
                            value.classification = "Batch Attack";
                        } else if (syntax.match(/^create( +function|procedure)/i)) {
                            value.classification = "Stored Procedure";
                        } else if (syntax.match(/concat(_ws)|(un)?(hex+)\W|substring\W|(([a-fA-F]|[0-9]){2}){10}/i)) {
                            value.classification = "Alternate Encoding";
                        } else if (syntax.match(/INSERT(\sINTO)|UPDATE(\s\w*)(\sSET\s)/i)) {
                            value.classification = "Second Order Injection";
                        } else if (syntax.match(/(')\W(\b(OR|ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)/i)) {
                            value.classification = "Ilegal/Incorrect Query Injection";
                        } else {
                            value.classification = "Unknown";
                        }
                        // value.connection = value.mysql_command_ref.connection;
                        // value.connection_timestamp = value.mysql_command_ref.connection_ref.connection_timestamp;
                        // value.remote_host = value.mysql_command_ref.connection_ref.remote_host;
                        // value.local_port = value.mysql_command_ref.connection_ref.local_port;
                        delete value.mysql_command_arg;
                        delete value.mysql_command;
                        delete value.mysql_command_arg_index;
                        delete value.mysql_command_ref;
                        if (syntax != '') {
                            _return.push( value.classification);
                        }
                    });
                    // if (_return.length<1) {
                    //     _return = [value._id, value.connection, value.connection_timestamp,
                    //         value.remote_host+" - "+geoip.lookup(value.remote_host).country, value.local_port, '', 'Unknown']
                    // }
                    return _return;
                },[]);
                var counts = {
                    'Tautological' :0,
                    'Batch Attack' :0,
                    'Stored Procedure' :0,
                    'Alternate Encoding' :0,
                    'Second Order Injection' :0,
                    'Ilegal/Incorrect Query Injection' :0,
                    'Unknown' :0,
                };
                for (var i = 0; i < _result.length; i++) {
                    counts[_result[i]] = 1 + (counts[_result[i]] || 0);
                }
                    console.log(counts);
                    io.emit('classification_table', counts);

                    labels = _result.reduce((acc, o) => (acc[o] = (acc[o] || 0) + 1, acc), {});

                    data = Object.values(labels).sort(function (a, b) { return a - b }).reverse();
                    labels = Object.keys(labels).sort(function (a, b) { return labels[a] - labels[b] }).reverse();
                    data = {
                        labels: labels,
                        datasets: [{
                            label: "Count",
                            backgroundColor: "rgba(2,117,216,1)",
                            borderColor: "rgba(2,117,216,1)",
                            data: data,
                        }],
                    };
                    io.emit('classification', data);
                    console.log('emit classification option 5');
                });
                res.send('ok');
            break;
        default:
            res.send('error');
            break;

    }
})

router.get('/scan_url', (req, res) => {
    // var url = req.body.url;
    var url = req.query['url'];
    // virusTotal.fileSearch(url).then((response) => {
    //     console.log(response);
        
    // }) //private api
    virusTotal.urlScan(url).then((response) => {
        res.send(response.permalink);
    })
})
module.exports = router;